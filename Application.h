//
// Created by zsolt on 2017.10.22..
//

#ifndef OAF_2_APPLICATION_H
#define OAF_2_APPLICATION_H

#include "Matrix.h"

class Application {
public:
    Matrix mat;

    void run(Matrix &s);

private:

    void edit(Matrix &s);

    void print(Matrix &s);

    void add(Matrix &s);

    void writeMenu();
};

#endif //OAF_2_APPLICATION_H
