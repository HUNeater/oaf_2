#include "Matrix.h"
#include "Application.h"

int main() {
    Application app;
    app.run(app.mat);

    return 0;
}