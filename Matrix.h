//
// Created by zsolt on 2017.10.15..
//

#ifndef OAF_2_MATRIX_H
#define OAF_2_MATRIX_H

#include <ostream>

struct Node {
    Node *next = nullptr;
    int value;
    unsigned int i;
    unsigned int j;

    Node(unsigned int i, unsigned int j, int value) : value(value), i(i), j(j) {};

    Node(unsigned int i, unsigned int j) : Node(i, j, 0) {};

    Node() : Node(0, 0, 0) {};
};

class Matrix {
public:
    Matrix();

    Matrix(const Matrix &m2);

    Matrix &operator=(const Matrix &rhs);

    ~Matrix();

    Matrix &operator+=(const Matrix &rhs);

    friend Matrix operator+(Matrix lhs, const Matrix &rhs);

    friend std::ostream &operator<<(std::ostream &out, Matrix &m);

    int &operator()(unsigned int i, unsigned int j);

private:
    Node *p;
};


#endif //OAF_2_MATRIX_H
