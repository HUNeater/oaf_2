//
// Created by zsolt on 2017.10.15..
//

#include <iostream>
#include "Matrix.h"

Matrix::Matrix() {
    p = new Node();
}

Matrix::Matrix(const Matrix &m2) {
    p = new Node();

    auto itr2 = m2.p;
    auto itr = p;

    itr->value = itr2->value;
    itr2 = itr2->next;

    while (itr2 != nullptr) {
        itr->next = new Node(itr2->i, itr2->j, itr2->value);
        itr = itr->next;
        itr2 = itr2->next;
    }
}

Matrix &Matrix::operator=(const Matrix &rhs) {
    p = new Node();

    auto itr2 = rhs.p;
    auto itr = p;

    itr->value = itr2->value;
    itr2 = itr2->next;

    while (itr2 != nullptr) {
        itr->next = new Node(itr2->i, itr2->j, itr2->value);
        itr = itr->next;
        itr2 = itr2->next;
    }

    return *this;
}

int &Matrix::operator()(unsigned int i, unsigned int j) {
    Node *itr = p->next;
    Node *prev = p;
    Node *e = nullptr;
    bool b1 = false;
    bool b2 = false;

    // search for i
    while (!b1) {
        if (itr == nullptr) {
            b1 = true;
            e = prev;
        } else if (itr->i < i && itr->next != nullptr) {
            prev = itr;
            itr = itr->next;
        } else if (itr->i > i) {
            b1 = true;
            e = prev;
        } else if (itr->i <= i) {
            b1 = true;
            e = itr;
        }
    }

    if (e->i == i) {

        itr = e;
        prev = p;
        while (prev->next != itr && prev->next != nullptr) prev = prev->next;

        // search for j
        while (!b2) {
            if (itr->j < j && itr->next != nullptr) {
                prev = itr;
                itr = itr->next;
            } else if (itr->j > j) {
                b2 = true;
                e = prev;
            } else if (itr->j <= j) {
                b2 = true;
                e = itr;
            }
        }
    }

    if (e->i != i || e->j != j) {
        Node *tmp = e->next;
        e->next = new Node(i, j);
        e->next->next = tmp;

        return e->next->value;
    }

    return e->value;
}

Matrix operator+(Matrix lhs, const Matrix &rhs) {
    lhs += rhs;
    return lhs;
}

Matrix &Matrix::operator+=(const Matrix &rhs) {
    auto itr1 = p;
    auto itr2 = rhs.p;

    Node *itr1_b = itr1;

    while (itr1 != nullptr && itr2 != nullptr) {
        itr1_b = itr1;

        if (itr1->i < itr2->i) {
            itr1 = itr1->next;
        } else if (itr1->i > itr2->i) {
            operator()(itr2->i, itr2->j) = itr2->value;
            itr2 = itr2->next;
        } else {
            if (itr1->j < itr2->j) {
                itr1 = itr1->next;
            } else if (itr1->j > itr2->j) {
                operator()(itr2->i, itr2->j) = itr2->value;
                itr2 = itr2->next;
            } else {
                operator()(itr1->i, itr1->j) = itr1->value + itr2->value;
                itr1 = itr1->next;
                itr2 = itr2->next;
            }
        }
    }

    // if RHS has remaining, add them
    if (itr1 == nullptr) {
        while (itr2 != nullptr) {
            itr1_b->next = new Node(itr2->i, itr2->j, itr2->value);
            itr1_b = itr1_b->next;
            itr2 = itr2->next;
        }
    }

    return *this;
}

std::ostream &operator<<(std::ostream &out, Matrix &m) {
    auto itr = m.p;

    while (itr != nullptr) {
        std::cout << "(" << itr->i << "," << itr->j << ")=" << itr->value << "; ";
        itr = itr->next;
    }

    return out;
}

Matrix::~Matrix() {
    while (p != nullptr) {
        Node *h = p;
        p = h->next;
        delete h;
    }
}
