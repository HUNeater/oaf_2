//
// Created by zsolt on 2017.09.28..
//

#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include "Matrix.h"

TEST_CASE("constructors & operator()") {
    Matrix mat;
    mat(0, 0) = 1;
    mat(1, 1) = 666;
    mat(1, 1) = 2;
    mat(2, 2) = 3;

    SECTION("() operator") {
        CHECK(mat(0, 0) == 1);
        CHECK(mat(1, 1) == 2);
        CHECK(mat(2, 2) == 3);
    }

    Matrix mat2(mat);
    mat2(3, 3) = 4;

    SECTION("copy constructor") {
        CHECK(mat2(0, 0) == 1);
        CHECK(mat2(1, 1) == 2);
        CHECK(mat2(2, 2) == 3);
        CHECK(mat2(3, 3) == 4);
    }

    Matrix mat3 = mat2;
    mat3(4, 4) = 5;

    SECTION("= operator") {
        CHECK(mat3(0, 0) == 1);
        CHECK(mat3(1, 1) == 2);
        CHECK(mat3(2, 2) == 3);
        CHECK(mat3(3, 3) == 4);
        CHECK(mat3(4, 4) == 5);
    }

    Matrix mat4;
    mat = mat4;

    SECTION("empty matrix with = operator") {
        CHECK(mat(0, 0) == 0);
    }

    Matrix mat5(mat);

    SECTION("empty matrix with copy constructor") {
        CHECK(mat5(0, 0) == 0);
    }
}

TEST_CASE("operator+ & operator+=") {
    Matrix mat;
    mat(0, 0) = 1;
    mat(1, 1) = 1;
    mat(2, 1) = 1;
    mat(2, 2) = 1;

    Matrix mat2;
    mat2(0, 0) = 1;
    mat2(1, 1) = 1;
    mat2(2, 2) = 1;
    mat2(3, 3) = 10;
    mat2(3, 2) = 10;

    Matrix mat3;
    mat3 = mat + mat2;

    SECTION("+ operator") {
        CHECK(mat3(0, 0) == 2);
        CHECK(mat3(1, 1) == 2);
        CHECK(mat3(2, 1) == 1);
        CHECK(mat3(2, 2) == 2);
        CHECK(mat3(3, 2) == 10);
        CHECK(mat3(3, 3) == 10);
    }

    Matrix mat4;
    mat4(0, 0) = 2;
    mat4(1, 1) = 2;
    mat4(2, 2) = 2;
    mat4(3, 0) = 2;
    mat4 += mat3;

    SECTION("+= operator") {
        CHECK(mat4(0, 0) == 4);
        CHECK(mat4(1, 1) == 4);
        CHECK(mat4(2, 1) == 1);
        CHECK(mat4(2, 2) == 4);
        CHECK(mat4(3, 0) == 2);
        CHECK(mat4(3, 2) == 10);
        CHECK(mat4(3, 3) == 10);
    }
}