//
// Created by zsolt on 2017.10.22..
//

#include <iostream>
#include "Application.h"

void Application::run(Matrix &s) {
    int n = 0;

    do {
        writeMenu();
        std::cin >> n;

        switch (n) {
            case 1:
                edit(s);
                break;
            case 2:
                add(s);
                break;
            case 3:
                print(s);
                break;

            default:
                break;
        }
    } while (n != 0);
}


void Application::edit(Matrix &s) {
    unsigned int i, j;
    int k;

    std::cout << "Edit the value at: ";
    std::cin >> i >> j;
    std::cout << " to: ";
    std::cin >> k;

    s(i, j) = k;
}

void Application::print(Matrix &s) {
    std::cout << s << std::endl;
}

void Application::add(Matrix &s) {
    Matrix s2;

    std::cout << "Edit the other matrix!" << std::endl;
    run(s2);

    s += s2;
}

void Application::writeMenu() {
    using namespace std;

    cout << "0 - exit" << endl;
    cout << "1 - edit" << endl;
    cout << "2 - add to other matrix" << endl;
    cout << "3 - print matrix" << endl;
}